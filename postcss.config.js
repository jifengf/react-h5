// import postcsspxtoviewport8plugin from "postcss-px-to-viewport-8-plugin";
import pxtorem from "postcss-pxtorem";
import autoprefixer from "autoprefixer";

export default {
  plugins: [
    autoprefixer({}),
    pxtorem({
      // /* 对应750设计稿，1rem = 10px， 且呼应index.html配置 */
      rootValue({ file }) {
        return file.indexOf("vant") !== -1 ? 5 : 10;
      },
      propList: ["*"] //propList*号表是所有的都要转
      // propList:['*','!font*','!line-height','!letter-spacing'], propList*号表是所有的都要转"! font*’表示排除 font*的css设置不转
    })
    // postcsspxtoviewport8plugin({
    //   unitToConvert: "px",
    //   viewportUnit: "vw",
    //   fontViewportUnit: "vw",
    //   unitPrecision: 5, // 单位转换后保留的精度
    //   propList: ["*"], // 能转化为vw的属性列表
    //   viewportWidth: 375,
    //   exclude: [/^(?!.*node_modules\/react-vant)/]
    // }),
    // postcsspxtoviewport8plugin({
    //   unitToConvert: "px",
    //   unitPrecision: 5, // 单位转换后保留的精度
    //   propList: ["*"], // 能转化为vw的属性列表
    //   viewportUnit: "vw",
    //   fontViewportUnit: "vw",
    //   viewportWidth: 750,
    //   exclude: [/node_modules\/react-vant/i]
    // })
  ]
};
