/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-26 15:54:50
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2024-01-10 10:57:55
 * @FilePath: \react-h5\vite.config.js
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */
import { defineConfig, loadEnv } from "vite";
import react from "@vitejs/plugin-react-swc";
import path from "path";
import svgr from "vite-plugin-svgr";

// https://vitejs.dev/config/
export default defineConfig(({ mode, command }) => {
  const env = loadEnv(mode, process.cwd());
  return {
    plugins: [react({}), svgr()],
    css: {
      // 预处理器配置项
      preprocessorOptions: {
        less: {
          math: "always"
        }
      }
    },
    resolve: {
      alias: {
        swiper: path.resolve(__dirname, "../../dist/"),
        "@": path.resolve(__dirname, "src"),
        "@assets": path.join(__dirname, "src/assets")
      }
    },
    server: {
      host: "0.0.0.0",
      port: 7000,
      // 是否开启 https
      // http://localhost:5173/api/login -> http://localhost:3001/login
      proxy: {
        '/BOCGIFTPIC': {
          target: 'https://mobile.jf365.boc.cn/BOCGIFTPIC',
          changeOrigin: true,
          rewrite: path => path.replace(RegExp(`^/BOCGIFTPIC`), '')
        },
      }
    }
  };
});
