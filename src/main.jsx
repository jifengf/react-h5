/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-26 15:54:50
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2024-01-04 15:48:45
 * @FilePath: \react-h5\src\main.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */
import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.less";

import "@/libs/rem.js";

// Import Swiper styles
import "../node_modules/swiper/swiper-bundle.css";
import "../node_modules/swiper/modules/navigation.css";
import "../node_modules/swiper/modules/pagination.css";

import { RootStoreProvider } from "@/store/index";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RootStoreProvider>
      <App />
    </RootStoreProvider>
  </React.StrictMode>
);
