/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-26 16:46:22
 * @LastEditors: wax
 * @LastEditTime: 2024-01-11 09:57:38
 * @FilePath: \react-h5\src\router\routes.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */

import BocHome from "@/pages/bocHome/Index";
import BocAccumulateScores from "@/pages/bocAccumulateScores/Index";
import StyledComponents from "@/pages/styledComponents/Index";
import Axios from "@/pages/axios/Index";
import IndexBar from "@/pages/vantIndexBar/Index";
import VantSwiper from "@/pages/vantSwiper/Index";
import Swiper from "@/pages/swiper/Index";
import UseHome from "@/pages/home/Index";
import Layout from "@/layout/Index";

const routes = [
  {
    path: "/",
    redirect: "/index",
    element: <Layout />,
    children: [
      {
        path: "/index",
        title: "首页",
        exact: true,
        element: <UseHome />
      },
      {
        path: "/swiper",
        title: "Swiper轮播组件",
        exact: true,
        element: <Swiper />
      },
      {
        path: "/vantSwiper",
        title: "Vant轮播组件",
        exact: true,
        element: <VantSwiper />
      },
      {
        path: "/indexBar",
        title: "Vant索引栏",
        exact: true,
        element: <IndexBar />
      },
      {
        path: "/axios",
        title: "Axios封装",
        exact: true,
        element: <Axios />
      },
      {
        path: "/styledComponents",
        title: "Styled 组件",
        exact: true,
        element: <StyledComponents />
      },
      // {
      //   path: "/bocHome",
      //   title: "中国银行 积分畅兑",
      //   exact: true,
      //   element: <BocHome />
      // },
      {
        path: "/bocAccumulateScores",
        title: "中国银行 积分畅兑",
        exact: true,
        element: <BocAccumulateScores />
      }
    ]
  },
  {
    path: "/bocHome",
    title: "中国银行 积分畅兑",
    exact: true,
    element: <BocHome />
  }
];

export default routes;
