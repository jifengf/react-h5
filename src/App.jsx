/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-26 15:54:50
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2024-01-08 10:47:58
 * @FilePath: \react-h5\src\App.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */

import React, { useEffect } from "react";
import {
  BrowserRouter as Router,
  HashRouter,
  Routes,
  Route,
  useRoutes
} from "react-router-dom";
import "./App.less";
import routes from "@/router/routes";
import { observer } from "mobx-react";
import { useStore } from "@/store/index";

const UseApp = observer(() => {
  const store = useStore();
  const { titleStore } = store;


  const makeElementTree = (arr, titleStore) => {
    // 构建一个内部函数，用于实现递归
    const makeTree = (arr, titleStore) => {
      arr.forEach((item) => {
        if (item.path && item.title) {
          titleStore.setMap(item.path, item.title);
        }
        if (item.children) {
          makeTree(item.children, titleStore);
        }
      });
    };
    return makeTree(arr, titleStore);
  };

  useEffect(() => {
    makeElementTree(routes, titleStore);
  }, []);
  const GetRoutes = () => useRoutes(routes); //一定要是函数内
  return (
    <HashRouter>
      <GetRoutes />
    </HashRouter>
  );
});

export default UseApp;
