const getImgUrl = (img) => {
  return new URL(`../assets/${img}`, import.meta.url).href;
};

export { handleLoadImage, getImgUrl };

const handleLoadImage = (arr) => {
  let imgObject = {};
  arr.map(item => {
    imgObject[item] = getImgUrl(item);
  })
  
  console.log("list", imgObject);
  return imgObject
}
