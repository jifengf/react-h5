/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-14 17:00:05
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-14 17:00:12
 * @FilePath: \react-h5-blog\public\config.ts
 * @Description: 
 * 
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved. 
 */
export const GLOBAL_API_URL = "https://szdapi.blwlkj.cn/api";
