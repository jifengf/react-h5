/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-14 16:32:13
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-26 16:26:40
 * @FilePath: \react-h5-vite\src\utils\request.js
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */
import axios from "axios";
import { GLOBAL_API_URL } from "./config";

// 创建 axios 实例
const service = axios.create({
  /* 基础地址 一般都是动态的 */
  baseURL: GLOBAL_API_URL,
  /* 请求类型定义 */
  // headers: {
  //   'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
  // },
  /* 如果用的JSONP，可以配置此参数带上cookie凭证，如果是代理和CORS不用设置 */
  withCredentials: false,
  /* 请求超时时间 */
  timeout: 6000
});

/**
 * http request 拦截器
 */
service.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("szd_token"); /* 获取token */
    if (token) {
      /* 让每个请求携带自定义 token 请根据实际情况自行修改 */
      config.headers["token"] = `${token}`;
    }
    if (config.method === "get") {
      /* 让每个请求都携带一个不同的时间参数，防止浏览器缓存不发送请求 */
      config.params = {
        ...config.params
      };
    }

    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

/**
 * http response 拦截器
 */
service.interceptors.response.use(
  (response) => {
    if (response.data.code !== 1) {
      return new Error(response.data.msg);
    }
    return response.data.data;
  },
  (error) => {
    return new Error("请求出错" + error);
  }
);

export default service;
