/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-27 09:26:44
 * @LastEditors: wax
 * @LastEditTime: 2024-01-11 09:56:06
 * @FilePath: \react-h5\src\layout\Index.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */
import React, { useEffect, useState } from "react";
import { useNavigate, useLocation, Outlet } from "react-router-dom";
import "./style.less";

import { Toast, NavBar } from "react-vant";
import { Search, WeappNav } from "@react-vant/icons";
import { observer } from "mobx-react";
import titleStore from "@/store/titleStore";

const Layout = observer(() => {
  // 得到store

  const nav = useNavigate();
  let location = useLocation();

  useEffect(() => {
    // 在路由变化时执行的操作
    if (location.pathname === "/index" || location.pathname === "/") return;
    let titleDom = document.getElementsByClassName("rv-nav-bar__title");
    if (!titleDom || !titleDom[0]) return;
    titleDom[0].innerHTML = titleStore.getMap(location.pathname);
  }, [location]);

  useEffect(() => {
    if (location.pathname === "/" || location.pathname === "/index") {
      nav("/index");
    }
  }, []);

  const handleBack = () => {
    Toast("返回");
    nav(-1); // 传入-1表示后退
  };

  const CommonHeader = () => {
    return (
      <NavBar
        title='React Vite H5 Demo'
        leftText='返回'
        onClickLeft={handleBack}
        rightText={<WeappNav />}
        onClickRight={() => Toast("按钮")}
      />
    );
  };

  return (
    <section className='layout-box'>
      {<CommonHeader />}
      <div className='layout-content'>
        <Outlet />
      </div>
      {/* <div className='layout-footer'>底部</div> */}
    </section>
  );
});

export default Layout;
