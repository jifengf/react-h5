/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-14 16:34:27
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-14 16:34:33
 * @FilePath: \react-h5-blog\src\api\index.ts
 * @Description: 
 * 
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved. 
 */
import user from "./modules/user";
import banner from "./modules/banner";
export default {
  user,
  banner
};
