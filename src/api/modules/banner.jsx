/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-14 16:34:48
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-14 16:34:59
 * @FilePath: \react-h5-blog\src\api\modules\banner.ts
 * @Description: 
 * 
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved. 
 */
import service from "@/utils/request";
export default {
  getBannerList(params) {
    return service({
      method: "get",
      url: "/common/banner",
      params
    });
  }
};
