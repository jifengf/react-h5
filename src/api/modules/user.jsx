import service from "@/utils/request";
export default {
  // 增加
  addUser(data) {
    return service({
      method: "post",
      url: "/v1/api/user",
      data
    });
  },

  // 获取用户
  getUser(params) {
    return service({
      method: "get",
      url: "/v1/api/user",
      params
    });
  }
};
