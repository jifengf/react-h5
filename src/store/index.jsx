import React from "react";

import titleStore from "./titleStore";

class RootStore {
  constructor() {
    this.titleStore = titleStore;
  }
}

// context机制的数据查找链  Provider如果找不到 就找createContext方法执行时传入的参数

const rootStore = new RootStore();
const RootStoreContext = React.createContext();

const RootStoreProvider = ({ children }) => {
  return (
    <RootStoreContext.Provider value={rootStore}>
      {children}
    </RootStoreContext.Provider>
  );
};

const useStore = () => React.useContext(RootStoreContext);

export { RootStoreProvider, useStore };
