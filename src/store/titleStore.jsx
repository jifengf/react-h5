import { makeAutoObservable } from "mobx";

class TitleStore {
  map = new Map();
  constructor() {
    makeAutoObservable(this);
  }

  setMap = (key, v) => {
    this.map.set(key, v);
  };

  getMap(key) {
    if (this.map.has(key)) {
      return this.map.get(key);
    } else {
      return "";
    }
  }
}

const titleStore = new TitleStore();

export default titleStore;
