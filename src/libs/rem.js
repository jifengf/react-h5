/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-25 15:19:21
 * @LastEditors: wax
 * @LastEditTime: 2024-01-11 09:50:56
 * @FilePath: \react-h5\src\libs\rem.js
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */
// /* 对应750设计稿，1rem = 10px， 且呼应.postcssrc.js配置 */
function setRem() {
  const baseFontSize = 10;
  const baseWidth = 750;
  const clientWidth =
    document.documentElement.clientWidth || window.innerWidth || 750;
  let innerWidth = clientWidth;
  let rem = 10;
  // if (innerWidth >= 800) {
  //   innerWidth = 800;
  // }
  rem = Math.floor((innerWidth / baseWidth) * baseFontSize);

  const dhtml = document.querySelector("html");
  dhtml && (dhtml.style.fontSize = rem + "px");
}
// 初始化
setRem();
// 改变窗口大小时重新设置 rem
window.onresize = function () {
  setRem();
};
