import UseNavigation from "@/components/swiperSlide/navigation/Index";
import UsePagination from "@/components/swiperSlide/pagination/Index";
import UsePaginationDynamic from "@/components/swiperSlide/paginationDynamic/Index";
import UsePaginationProgress from "@/components/swiperSlide/paginationProgress/Index";
import UsePaginationFraction from "@/components/swiperSlide/paginationFraction/Index";
import UsePaginationCustom from "@/components/swiperSlide/paginationCustom/Index";
import UseScrollbar from "@/components/swiperSlide/scrollbar/Index";
import UseVertical from "@/components/swiperSlide/vertical/Index";
import UseSpaceBetween from "@/components/swiperSlide/spaceBetween/Index";
import UseSlidesPerView from "@/components/swiperSlide/slidesPerView/Index";
import UseSlidesPerViewAuto from "@/components/swiperSlide/slidesPerViewAuto/Index";
import UseCentered from "@/components/swiperSlide/centered/Index";
import UseCenteredAuto from "@/components/swiperSlide/centeredAuto/Index";
import UseCssModefrom from "@/components/swiperSlide/cssMode/Index";
import UseFreemode from "@/components/swiperSlide/freemode/Index";
import UseScrollContainer from "@/components/swiperSlide/scrollContainer/Index";
import UseGrid from "@/components/swiperSlide/grid/Index";
import UseNested from "@/components/swiperSlide/nested/Index";
import UseGrabCursor from "@/components/swiperSlide/grabCursor/Index";
import UseInfiniteLoop from "@/components/swiperSlide/infiniteLoop/Index";
import UseSlidesPerGroupSkip from "@/components/swiperSlide/slidesPerGroupSkip/Index";
import UseEffectFade from "@/components/swiperSlide/effectFade/Index";
import UseEffectCube from "@/components/swiperSlide/effectCube/Index";
import UseEffectCoverflow from "@/components/swiperSlide/effectCoverflow/Index";
import UseEffectCards from "@/components/swiperSlide/effectCards/Index";
import UseEffectCreative from "@/components/swiperSlide/effectCreative/Index";
import UseKeyboardControl from "@/components/swiperSlide/keyboardControl/Index";
import UseMousewheelControl from "@/components/swiperSlide/mousewheelControl/Index";
import UseAutoplay from "@/components/swiperSlide/autoplay/Index";
import UseManipulation from "@/components/swiperSlide/manipulation/Index";
import UseParallax from "@/components/swiperSlide/parallax/Index";
import UseLazyLoadImages from "@/components/swiperSlide/lazyLoadImages/Index";
import UseVirtualSlides from "@/components/swiperSlide/virtualSlides/Index";
import UseRewind from "@/components/swiperSlide/rewind/Index";
import UseChangeDirection from "@/components/swiperSlide/changeDirection/Index";
import UseAlmostDemo from "@/components/swiperSlide/almostDemo/Index";

import UseStyledComponents from "@/pages/styledComponents/Index";

import "./style.less";
import { Grid, Image, Cell } from "react-vant";

const Swiper = () => {
  const handleNavTo = () => {
    window.open("https://v8.swiperjs.com/react", "_blank", "noreferrer");
  };
  return (
    <div className="swiper-home">
      <Cell.Group card border>
        <Cell title="Almost Demo">
          <div onClick={handleNavTo}>Swiper React 官网</div>
        </Cell>
        <Cell>
          <UseAlmostDemo></UseAlmostDemo>
        </Cell>
      </Cell.Group>

      <UseStyledComponents></UseStyledComponents>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Change direction" />
          <Cell>
            <UseChangeDirection></UseChangeDirection>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Rewind" />
          <Cell>
            <UseRewind></UseRewind>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Virtual slides" />
          <Cell>
            <UseVirtualSlides></UseVirtualSlides>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Lazy load images(待学习)" />
          <Cell>
            <UseLazyLoadImages></UseLazyLoadImages>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Parallax(待优化)" />
          <Cell>
            <UseParallax></UseParallax>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Manipulation (dynamic slides)" />
          <Cell>
            <UseManipulation></UseManipulation>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Autoplay" />
          <Cell>
            <UseAutoplay></UseAutoplay>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Mousewheel control" />
          <Cell>
            <UseMousewheelControl></UseMousewheelControl>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Keyboard control" />
          <Cell>
            <UseKeyboardControl></UseKeyboardControl>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Effect creative" />
          <Cell>
            <UseEffectCreative></UseEffectCreative>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Effect cards" />
          <Cell>
            <UseEffectCards></UseEffectCards>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Effect coverflow" />
          <Cell>
            <UseEffectCoverflow></UseEffectCoverflow>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Effect cube" />
          <Cell>
            <UseEffectCube></UseEffectCube>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Effect fade" />
          <Cell>
            <UseEffectFade></UseEffectFade>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Slides per group skip" />
          <Cell>
            <UseSlidesPerGroupSkip></UseSlidesPerGroupSkip>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Infinite Loop" />
          <Cell>
            <UseInfiniteLoop></UseInfiniteLoop>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Grab cursor" />
          <Cell>
            <UseGrabCursor></UseGrabCursor>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Nested" />
          <Cell>
            <UseNested></UseNested>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Grid" />
          <Cell>
            <UseGrid></UseGrid>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Scroll container" />
          <Cell>
            <UseScrollContainer></UseScrollContainer>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Free Mode" />
          <Cell>
            <UseFreemode></UseFreemode>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Css Mode" />
          <Cell>
            <UseCssModefrom></UseCssModefrom>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Centered auto" />
          <Cell>
            <UseCenteredAuto></UseCenteredAuto>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Centered" />
          <Cell>
            <UseCentered></UseCentered>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Slides per view auto" />
          <Cell>
            <UseSlidesPerViewAuto></UseSlidesPerViewAuto>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Slides per view" />
          <Cell>
            <UseSlidesPerView></UseSlidesPerView>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Space between" />
          <Cell>
            <UseSpaceBetween></UseSpaceBetween>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Vertical" />
          <Cell>
            <UseVertical></UseVertical>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Navigation" />
          <Cell>
            <UseNavigation></UseNavigation>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Pagination" />
          <Cell>
            <UsePagination></UsePagination>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Pagination dynamic" />
          <Cell>
            <UsePaginationDynamic></UsePaginationDynamic>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Pagination progress" />
          <Cell>
            <UsePaginationProgress></UsePaginationProgress>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Pagination fraction" />
          <Cell>
            <UsePaginationFraction></UsePaginationFraction>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Pagination custom" />
          <Cell>
            <UsePaginationCustom></UsePaginationCustom>
          </Cell>
        </Cell.Group>
      </div>
      <div className="card-box">
        <Cell.Group card border>
          <Cell title="Scrollbar" />
          <Cell>
            <UseScrollbar></UseScrollbar>
          </Cell>
        </Cell.Group>
      </div>
    </div>
  );
};

export default Swiper;
