import React from "react";
import { IndexBar, Tabs, Cell } from "react-vant";

const indexList = [];
const customIndexList = [1, 2, 3, 4, 5, 6, 8, 9, 10];
const charCodeOfA = "A".charCodeAt(0);

for (let i = 0; i < 26; i += 1) {
  indexList.push(String.fromCharCode(charCodeOfA + i));
}

const handleNavTo = () => {
  window.open(
    "https://react-vant.3lang.dev/components/index-bar",
    "_blank",
    "noreferrer"
  );
};

const useIndexbar = () => {
  return (
    <div className="indexbar-home">
      <Tabs>
        <Tabs.TabPane title="基础用法">
          <IndexBar>
            {indexList.map((item) => (
              <div key={item}>
                <IndexBar.Anchor index={item} style={{ textAlign: "left" }}>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                      color: "#969799"
                    }}
                  >
                    {item}
                    {item === "A" && (
                      <div onClick={handleNavTo}>Vant IndexBar (官网)</div>
                    )}
                  </div>
                </IndexBar.Anchor>
                <Cell title="文本" />
                <Cell title="文本" />
                <Cell title="文本" />
              </div>
            ))}
          </IndexBar>
        </Tabs.TabPane>
        <Tabs.TabPane title="自定义索引列表">
          <IndexBar indexList={customIndexList}>
            {customIndexList.map((item) => (
              <div key={item}>
                <IndexBar.Anchor index={item} style={{ textAlign: "left" }}>
                  <div>标题 {item}</div>
                </IndexBar.Anchor>
                <Cell title="文本" />
                <Cell title="文本" />
                <Cell title="文本" />
              </div>
            ))}
          </IndexBar>
        </Tabs.TabPane>
      </Tabs>
    </div>
  );
};

export default useIndexbar;
