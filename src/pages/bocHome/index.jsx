/*
 * @Author:  hpu@foxmail.com
 * @Date: 2024-01-10 11:07:57
 * @LastEditors: wax
 * @LastEditTime: 2024-01-11 15:41:56
 * @FilePath: \react-h5\src\pages\bocHome\index.jsx
 * @Description:
 *
 * Copyright (c) 2024 by hpu@foxmail.com, All Rights Reserved.
 */
import React, { useState, useEffect, useRef } from "react";
import { useNavigate, useLocation, Outlet } from "react-router-dom";

import {
  IndexBar,
  Tabs,
  Cell,
  Toast,
  Button,
  Grid,
  NoticeBar,
  Swiper,
  Image,
  Card
} from "react-vant";
import {
  ChatO,
  ShopO,
  Plus,
  Search as SearchIcon,
  WarningO,
  VolumeO,
  ArrowLeft
} from "@react-vant/icons";
import { mobileMenu, newsList, hotRang } from "@/utils/const";

import news from "@/assets/公告.png";
import react from "@/assets/react.svg";

import "./style.less";

const UseBocHome = () => {
  const [color, setColor] = useState("#fff");
  // const [color, setColor] = useState("#979393");
  const [bgColor, setBgColor] = useState("");
  const [sColor, setSColor] = useState("#f2f2f2");
  const [isSHow, setIsShow] = useState(false)

  const [domH, setDomH] = useState(300);
  const [cardDom, setCardDom] = useState(null);

  const nav = useNavigate();

  useEffect(() => {
    const cardDom = document.getElementsByClassName("rv-card--round");
    setCardDom(cardDom);
    let cardRect = cardDom[0].getBoundingClientRect();
    console.log("cardRect", cardRect);
    setDomH(cardRect.height);
  }, []);

  const handleNavTo = () => {
    window.open("https://axios-http.com/docs/intro", "_blank", "noreferrer");
  };

  const onChange = (e) => {
    Toast(`当前索引${e}`);
    if (cardDom && cardDom[0]) {
      let cardRect = cardDom[e].getBoundingClientRect();
      setDomH(cardRect.height);
    }
  };

  const onClick = (e) => {
    Toast(`${e.element_name}`);
    if (e.element_name === "全部分类") {
      nav("/bocAccumulateScores");
    }
  };

  const handleScroll = (event) => {
    console.log("ssss", event.currentTarget.scrollTop);
    if (event.currentTarget.scrollTop === 0) {
      setColor("#fff");
      setBgColor("");
      setSColor("#fff");
    } else {
      if (color !== "#979393") {
        setColor("#979393");
        setBgColor("#fff");
        setSColor("#f2f2f2");
      }
    }
  };

  const CardComp = (item, index) => {
    return (
      <Card round>
        <Grid border={false} columnNum={4}>
          {mobileMenu.data.data.childrens
            .slice(0, 8 + index * 3)
            .map((item, i) => (
              <Grid.Item
                onClick={() => onClick(item)}
                key={i}
                // icon={<img src={react} />}
                icon={<img src={`https://mobile.jf365.boc.cn/BOCGIFTPIC/image/${item.element_pic}`} />}
                text={item.element_name}
              />
            ))}
        </Grid>
      </Card>
    );
  };

  const NewsNoticeBar = () => {
    return (
      <div className='news-notice-bar'>
        <NoticeBar
          background='#fff'
          scrollable={false}
          leftIcon={<img src={news} />}
        >
          <Swiper
            autoplay={3000}
            indicator={false}
            vertical
            className='notice-swipe'
          >
            {newsList.data.data.childrens.map((item, index) => {
              return (
                <Swiper.Item key={index}>
                  <div className='swiper-item'>
                    <div className='dot'></div>
                    <div className='title'>{item.TITLE}</div>
                  </div>
                </Swiper.Item>
              );
            })}
          </Swiper>
        </NoticeBar>
      </div>
    );
  };

  const Header = () => {
    return (
      <div className='header' style={{ backgroundColor: bgColor }}>
        <div className='header-main'>
          <div className='search' style={{ backgroundColor: sColor }}>
            请输入搜索礼品名称搜索
            <div className='search-icon'>
              <SearchIcon />
            </div>
          </div>
          <div className='search-action'>
            <ChatO color={color} fontSize='42px' />
            <Plus color={color} fontSize='42px' />
          </div>
        </div>
      </div>
    );
  };

  const DonateCom = () => {
    return (
      <div className='item'>
        <div className='item-content'>
          <div
            className='one-image'
            style={{
              backgroundImage:
                "url(https://mobile.jf365.boc.cn/BOCGIFTPIC/image/2023060814431853.png)"
            }}
          ></div>
        </div>
      </div>
    );
  };

  const HotRant = () => {
    return (
      <div className='item hot-rant'>
        <div className='item-header'>热门排行榜</div>
        <div className='item-content'>
          <div className='rank-box'>
            {hotRang.map((item, index) => {
              return (
                <div
                  key={index}
                  className={`rank-content ${index == 0 ? "item-1" : "item-2"}`}
                >
                  <Image src={item.bgImg} />
                  <div className='rankingItem'>
                    <div className='rank-title'>
                      <img src={item.img} />
                      {item.title}
                    </div>
                    <div className='ranking-goods-box'>
                      {item.children.map((cont, key) => {
                        return (
                          <div className='goods-item' key={key}>
                            <img src={'https://mobile.jf365.boc.cn' + cont.OPICURL} />
                            <div className='rank-info'>
                              <div className='rank-info-no'>NO.{key + 1}</div>
                              <div className='rank-info-name'>
                                {cont.GIFTNAME}
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  };

  const SuggestComp = () => {
    return (
      <div className='item'>
        <div className='item-header'>推荐专区</div>
        <div className='suggest-comp'>
          <img
            src='https://mobile.jf365.boc.cn/BOCGIFTPIC/image/2023060814435335.png'
            className='suggest-left'
          ></img>
          <div className='suggest-right'>
            <img
              src='https://mobile.jf365.boc.cn/BOCGIFTPIC/image/2023060814435318.png'
              className='suggest-right-top'
            ></img>
            <img
              src='https://mobile.jf365.boc.cn/BOCGIFTPIC/image/2023060814435374.png'
              className='suggest-right-bottom'
            ></img>
          </div>
        </div>
      </div>
    );
  };
  return (
    <div className='boc-home' onClick={() => setIsShow(!isSHow)} onScroll={handleScroll}>
      {<Header />}
      <div className='integral'>
        <div className='rule'>
          平台规则
          <WarningO />
        </div>
        <div className='btn'>查看积分</div>
      </div>

      {/* // 公告新闻  */}
      <div>
        <NewsNoticeBar />
      </div>

      <div className='item'>
        <Swiper
          onChange={onChange}
          style={{ height: domH }}
          indicator={(total, current) => (
            <div className='custom-indicator'>
              {Array.from({ length: total }, (_, i) => (
                <div
                  key={i}
                  className={`indicator-index ${
                    current === i ? "indicator-index-active" : ""
                  }`}
                />
              ))}
            </div>
          )}
        >
          {[1, 2].map((item, index) => (
            <Swiper.Item key={item}>{CardComp(item, index)}</Swiper.Item>
          ))}
        </Swiper>
      </div>
      <DonateCom />
      <HotRant />
      <SuggestComp />

      <div className='item msgipv6'>支持IPV6网络</div>
      <div className='return-home'>
        {isSHow && <ArrowLeft onClick={ () => nav('/')} />}
      </div>
    </div>
  );
};

export default UseBocHome;
