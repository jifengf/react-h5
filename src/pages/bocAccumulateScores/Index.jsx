import React, { useState, useEffect, useRef } from "react";
import { Search, Toast, Sidebar, Tabs } from "react-vant";
import { ArrowDown } from "@react-vant/icons";

import { menu, giftList } from "@/utils/const";
import react from "@/assets/react.svg";

import "./style.less";

const UseBocAccumulateScores = () => {
  const [active, setActive] = useState(2);
  const [tabsActive, setTabsActive] = useState(0);
  const tabsRef = useRef(null);
  const searchRightRef = useRef(null);
  const [refArr, setRefArr] = useState([]);
  let [tabsContDom, setTabsContDom] = useState();

  useEffect(() => {
    // 创建一个新的数组来存储ref
    const arr = [];
    // 动态创建ref
    for (let i = 0; i < [1, 2, 3, 4, 5, 6, 7, 8].length; i++) {
      arr.push(React.createRef());
    }

    setRefArr(arr);
  }, []);

  useEffect(() => {
    const tabsWrapDom = document.getElementsByClassName("rv-tabs__wrap");
    const tabsDom = document.getElementsByClassName("rv-tabs");
    const _tabsContDom = document.getElementsByClassName("rv-tabs__content");
    setTabsContDom(_tabsContDom[0]);

    _tabsContDom[0].style.height =
      tabsDom[0].getBoundingClientRect().height -
      tabsWrapDom[0].getBoundingClientRect().height +
      "px";

    _tabsContDom[0].addEventListener("scroll", handleScroll);
    return () => {
      tabsContDom && tabsContDom.removeEventListener("scroll", handleScroll);
      setTabsContDom(null);
    };
  }, []);

  const handleScroll = (e) => {
    const tabsWrapDom = document.getElementsByClassName("rv-tabs__wrap");
    let tabsWrapDomRect = tabsWrapDom[0].getBoundingClientRect();
    const list = document.getElementsByClassName("rv-tab__pane");

    for (let i = 0; i < list.length; i++) {
      let top =
        list[i].getBoundingClientRect().top -
        tabsWrapDomRect.top -
        tabsWrapDomRect.height;

      if (top == 0) {
        setTabsActive(i);
        return;
      }

      if (i > 0 && top > 0) {
        let top_0 =
          list[i - 1].getBoundingClientRect().top -
          tabsWrapDomRect.top -
          tabsWrapDomRect.height;
        if (top_0 < 0) {
          setTabsActive(i - 1);
        }
      }
    }

    // 滚动到底部的时候，设置最后一个高亮
    const lastRect = list[list.length - 1].getBoundingClientRect();
    const distanceToBottom = window.innerHeight - (lastRect.bottom);
    if (distanceToBottom >= 0) {
      setTabsActive(list.length - 1);
    }
  };

  const handleClickTab = (item) => {
    if (item.name == 0) {
      tabsContDom.scrollTop = 0;
      return;
    }
    let topRect = 0;
    for (let i = 0; i < [1, 2, 3, 4, 5, 6, 7, 8].length; i++) {
      if (i < item.index) {
        const rect = refArr[i].current.getBoundingClientRect(); // 获取控件位置信息
        topRect = topRect + rect.height;
      }
    }
    tabsContDom.scrollTop = topRect;
    // tabsRef.current.scrollTo(item.name);
  };

  return (
    <div className='boc-scores-home'>
      <div className='search'>
        <Search
          shape='round'
          placeholder='请输入搜索关键词'
          onClickInput={() => {
            Toast("点击输入区域时触发	");
          }}
        />
      </div>

      <div className='searchWrap'>
        <div className='search-left'>
          <Sidebar
            value={active}
            onChange={(v) => {
              setActive(v);
              Toast.info(`标签名 ${v + 1}`);
            }}
          >
            {menu.data.giftClassInfos.map((item, index) => {
              return (
                <Sidebar.Item key={item.ici_classNO} title={item.ici_nameCN} />
              );
            })}
          </Sidebar>
        </div>

        <div className='search-right' ref={searchRightRef}>
          <div className='tabs-content'>
            <Tabs
              active={tabsActive}
              onClickTab={handleClickTab}
              ref={tabsRef}
              scrollspy={{ autoFocusLast: true, reachBottomThreshold: 50 }}
            >
              {giftList.data.map((item, index) => (
                <Tabs.TabPane
                  key={index}
                  title={`${item.classInfo.ici_nameCN}`}
                >
                  <div className='tab-pane-content' ref={refArr?.[index]}>
                    <div className='kind-content'>
                      <div className='kind-title'>
                        <div className='title'>
                          {item.classInfo.ici_nameCN}({item.gift.length})
                        </div>
                        <div className='int'>积分值</div>
                      </div>
                      <div className='gift-list'>
                        {item.gift.map((content, index) => {
                          return (
                            <div className='gift-item' key={index}>
                              <img src={content.lpicUR1L || react} className='img' />
                              <div className='other-info'>
                                <div className='name'>{content.giftName}</div>
                                <div className='value'>
                                  {content.giftValue}
                                  <span>积分</span>
                                </div>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </Tabs.TabPane>
              ))}
            </Tabs>
            <div className='icon-content'>
              <ArrowDown />
            </div>
          </div>
          {/* <div className='fixedTitle'>
            <div className='title'>电脑通讯</div>
            <div className='int'>积分值</div>
          </div> */}
        </div>
      </div>
    </div>
  );
};

export default UseBocAccumulateScores;
