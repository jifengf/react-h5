import { useState } from "react";
import "./style.less";
import { Grid, Image, Cell, Swiper, Toast } from "react-vant";
import { useNavigate, useLocation, Outlet } from "react-router-dom";

import { items } from "@/components/vantSwiper/items";
import Lazyload from "@/components/vantSwiper/lazyload";

const VantSwiper = () => {
  const nav = useNavigate();

  const [list, setList] = useState([
    {
      imgUrl: "https://swiperjs.com/images/swiper-logo.svg",
      title: "Swiper",
      key: "swiper",
      notice: "轮播组件库",
      path: "/swiper"
    }
  ]);

  const handleNavTo = () => {
    window.open(
      "https://react-vant.3lang.dev/components/swiper",
      "_blank",
      "noreferrer"
    );
  };

  return (
    <div className="vant-home">
      <Cell.Group card border>
        <Cell title="基础用法">
          <div onClick={handleNavTo}>Vant 轮播（官网）</div>
        </Cell>
        <Cell>
          <Swiper autoplay={3000}>{items}</Swiper>
        </Cell>
      </Cell.Group>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="懒加载" />
          <Cell>
            <Lazyload></Lazyload>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="监听onchange事件" />
          <Cell>
            <Swiper onChange={(i) => Toast(`当前索引${i}`)} autoplay={3000}>
              {items}
            </Swiper>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="纵向滚动" />
          <Cell>
            <Swiper vertical style={{ height: 150 }} autoplay={3000}>
              {items}
            </Swiper>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="自定义滑块大小，及居中" />
          <Cell>
            <Swiper slideSize={80} trackOffset={10}>
              {items}
            </Swiper>
          </Cell>
        </Cell.Group>
      </div>

      <div className="card-box">
        <Cell.Group card border>
          <Cell title="自定义指示器" />
          <Cell>
            <Swiper
              indicator={(total, current) => (
                <div className="custom-indicator">
                  {current + 1}/{total}
                </div>
              )}
            >
              {items}
            </Swiper>
          </Cell>
        </Cell.Group>
      </div>
    </div>
  );
};

export default VantSwiper;
