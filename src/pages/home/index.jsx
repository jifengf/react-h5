/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-26 16:41:35
 * @LastEditors: wax
 * @LastEditTime: 2024-01-11 15:39:54
 * @FilePath: \react-h5\src\pages\home\index.jsx
 * @Description:
 *
 * Copyright (c) 2024 by hpu@foxmail.com, All Rights Reserved.
 */
import { useState, useRef, useEffect, useLayoutEffect } from "react";
import "./style.less";
import { Grid, Image, Cell } from "react-vant";
import { useNavigate, useLocation, Outlet } from "react-router-dom";
import { useDebounceFn } from "ahooks";

import react from "@/assets/react.svg";
import ahooks from "@/assets/logo/ahooks.svg";
import axios from "@/assets/logo/axios.svg";
import styled from "@/assets/logo/styled.png";
import swiper from "@/assets/logo/swiper.svg";
import vant from "@/assets/logo/vant.svg";
import mobx from "@/assets/logo/mobx.png";
import boc from "@/assets/logo/boc.png";

const getImgUrl = (img) => {
  return new URL(`../../assets/logo/${img}`, import.meta.url).href;
};
const UseHome = () => {
  const homeRef = useRef(null);
  const nav = useNavigate();

  const [list, setList] = useState([
    {
      imgUrl: swiper,
      title: "Swiper",
      key: "swiper",
      notice: "轮播组件库",
      path: "/swiper"
    },
    {
      imgUrl: vant,
      title: "VantSwiper",
      key: "vantSwiper",
      notice: "Vant轮播组件库",
      path: "/vantSwiper"
    },
    {
      imgUrl: vant,
      title: "Vant索引栏",
      key: "IndexBar",
      notice: "索引栏",
      path: "/indexBar"
    },
    {
      imgUrl: ahooks,
      title: "React Hooks 库",
      key: "ahooks",
      notice: "Ahooks",
      path: "https://ahooks.js.org/zh-CN/hooks/use-request/index"
    },
    {
      imgUrl: axios,
      title: "AXIOS",
      key: "axios",
      notice: "AXIOS",
      path: "/axios"
    },
    {
      imgUrl: styled,
      title: "Styled 组件",
      key: "styled-components",
      notice: "styled-components",
      path: "/styledComponents"
    },
    {
      imgUrl: getImgUrl("mobx.png"),
      title: "Mobx 状态管理",
      key: "mobx",
      notice: "mobx 状态管理",
      path: "https://zh.mobx.js.org/README.html"
    },
    {
      imgUrl: react,
      title: "React",
      key: "React",
      notice: "React",
      path: "https://react.dev/"
    },
    {
      imgUrl: boc,
      title: "BOC积分兑换",
      key: "BOC",
      notice: "中国银行 积分畅兑",
      path: "/bocHome"
    }
  ]);

  // 防抖
  const { run } = useDebounceFn(
    (item) => {
      if (item.path.indexOf("http") !== -1) {
        window.open(item.path, "_blank", "noreferrer");
      } else {
        nav(item.path);
      }
    },
    { wait: 500 }
  );

  useLayoutEffect(() => {
    console.log("offsetHeight", homeRef.current.offsetHeight);
  }, []);

  return (
    <div className="home" style={{ background: `url(${react})` }} ref={homeRef}>
      <Cell.Group card border style={{ paddingTop: 15 }}>
        <Grid columnNum={3}>
          {list.map((item, index) => {
            return (
              <Grid.Item key={index} onClick={() => run(item)}>
                <Image width="80px" height="80px" src={item.imgUrl} />
                <div className="image-title">{item.title}</div>
              </Grid.Item>
            );
          })}
        </Grid>
        {/* <UseBannerSwiper></UseBannerSwiper> */}
      </Cell.Group>
      <img src={react} alt="" className="icon-react" />
      <img src={"src/assets/react.svg"} alt="" className="icon-react" />
    </div>
  );
};

export default UseHome;
