import React, { useEffect, useState } from "react";
import { IndexBar, Tabs, Cell } from "react-vant";

import {
  Swiper,
  SwiperSlide
} from "../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  EffectCoverflow,
  Keyboard,
  Autoplay,
  Virtual,
  EffectCreative
} from "../../../node_modules/swiper/modules";
import axios from "axios";
import "./style.less";
import styled from "styled-components";

const StyledComponContent = styled.div`
margin-top: 30px;
`;

const handleNavTo = () => {
  window.open("https://styled-components.com/", "_blank", "noreferrer");
};

const UseStyledComponents = () => {
  let [list, setList] = useState([]);

  useEffect(() => {
    axios
      .get("/svgIconTop.txt")
      .then((response) => {
        setList(response.data.split("###"));
      })
      .catch((error) => {
        // 处理错误
        console.log(error);
      });
  }, []);


  return (
    <StyledComponContent>
      <div className="styled-components-content">
        <Cell.Group card border>
          <Cell title="Swiper Demo">
            <div onClick={handleNavTo}>Styled-Components 官网</div>
          </Cell>
          <Cell>
            <Swiper
            observer= {true}
            observeParents= {true}
              spaceBetween={30}
              modules={[Virtual, Autoplay]}
              // initialSlide={0}
              // onSwiper={(swiper) => (window.swiper = swiper)}
              slidesPerView={3} // 展示数量
              centeredSlides={true}
              virtual={true}
              speed={1500}
              // loop={true}
              autoplay={{
                delay: 0,
                stopOnLastSlide: false,
                // pauseOnMouseEnter: true,
                disableOnInteraction: false
              }}
            >
              {list.map((item, index) => {
                return (
                  <SwiperSlide virtualIndex={index} key={index}>
                    <div
                      className="html-content"
                      key={index}
                      dangerouslySetInnerHTML={{ __html: item }}
                    />
                  </SwiperSlide>
                );
              })}
            </Swiper>
          </Cell>
        </Cell.Group>
      </div>
    </StyledComponContent>
  );
};

export default UseStyledComponents;
