import React from "react";
import { IndexBar, Tabs, Cell } from "react-vant";

import UseBannerSwiper from "@/components/bannerSwiper";
import "./style.less";

const UseAxios = () => {

  const handleNavTo = () => {
    window.open("https://axios-http.com/docs/intro", "_blank", "noreferrer");
  };

  return (
    <div className="axios-home">
      <Cell.Group card border>
        <Cell title="AXIOS Demo">
          <div onClick={handleNavTo}>Axios 官网</div>
        </Cell>
        <Cell>
          <UseBannerSwiper></UseBannerSwiper>
        </Cell>
      </Cell.Group>
    </div>
  );
};

export default UseAxios;
