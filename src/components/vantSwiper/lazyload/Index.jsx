import React from "react";
import { Swiper, Toast, Image } from "react-vant";
import "./style.less";

const images = [
  "https://img.yzcdn.cn/vant/apple-1.jpg",
  "https://img.yzcdn.cn/vant/apple-2.jpg",
  "https://img.yzcdn.cn/vant/apple-3.jpg",
  "https://img.yzcdn.cn/vant/apple-4.jpg",
  "https://img.yzcdn.cn/vant/apple-5.jpg",
  "https://img.yzcdn.cn/vant/apple-6.jpg",
  "https://img.yzcdn.cn/vant/apple-7.jpg",
  "https://img.yzcdn.cn/vant/apple-8.jpg"
];

const Lazyload = () => (
  <div className="swiper-item">
    <Swiper autoplay={4000}>
      {images.map((image) => (
        <Swiper.Item key={image}>
          <Image lazyload src={image} />
        </Swiper.Item>
      ))}
    </Swiper>
  </div>
);
export default Lazyload;
