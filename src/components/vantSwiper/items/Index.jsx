import React from "react";
import { Swiper, Toast } from "react-vant";
import "./style.less";

const colors = ["#ace0ff", "#bcffbd", "#e4fabd", "#ffcfac"];

export const items = colors.map((color, index) => (
  <Swiper.Item key={color}>
    <div
      className="swiper-item"
      style={{ backgroundColor: color }}
      onClick={() => {
        Toast.info(`你点击了卡片 ${index + 1}`);
      }}
    >
      {index + 1}
    </div>
  </Swiper.Item>
));
