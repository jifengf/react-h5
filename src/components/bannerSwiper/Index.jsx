/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState } from "react";
import { Swiper, SwiperSlide } from "../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  EffectCoverflow,
  Keyboard,
  Autoplay,
  EffectCreative
} from "../../../node_modules/swiper/modules";

import api from "@/api/index";

import "./style.less";

const UseBannerSwiper = () => {
  const [bannerList, setBannerList] = useState([]);

  useEffect(() => {
    api.banner.getBannerList({ goods_type_id: "" }).then((data) => {
      setBannerList(data);
      console.log("data =>", data);
    });
  }, []);

  return (
    <Swiper
      spaceBetween={50}
      modules={[EffectCreative, Pagination, Navigation, Keyboard, Autoplay]}
      initialSlide={0}
      // onSwiper={(swiper) => (window.swiper = swiper)}
      effect="creative"
      grabCursor={true}
      navigation={true}
      keyboard={{
        enabled: true
      }}
      centeredSlides={true}
      autoplay={{
        delay: 1000,
        pauseOnMouseEnter: true,
        disableOnInteraction: false
      }}
      pagination={{ clickable: true }}
      creativeEffect={{
        prev: {
          shadow: true,
          origin: "left center",
          translate: ["-5%", 0, -200],
          rotate: [0, 100, 0]
        },
        next: {
          origin: "right center",
          translate: ["5%", 0, -200],
          rotate: [0, -100, 0]
        }
      }}
      // onSlideChange={() => console.log("slide change")}
      // onSwiper={(swiper) => console.log(swiper)}
    >
      {bannerList.map((item) => {
        return (
          <SwiperSlide key={item.id}>
            <img className="swiper-img" src={item.image}></img>
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
};

export default UseBannerSwiper;
