/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-28 15:02:54
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-29 09:36:56
 * @FilePath: \react-h5\src\components\swiperSlide\autoplay\Index.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */
import React from "react";
import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  EffectCoverflow,
  Keyboard,
  Autoplay,
  EffectCreative
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseAutoplay = () => {
  return (
    <div className='autoplay-content'>
      <Swiper
        modules={[EffectCreative, Pagination, Navigation,Keyboard,Autoplay]}
        initialSlide={0}
        // onSwiper={(swiper) => (window.swiper = swiper)}
        effect='creative'
        grabCursor={true}
        navigation={true}
        keyboard={{
          enabled: true
        }}
        centeredSlides={true}
        autoplay={{
          delay: 1000,
          pauseOnMouseEnter: true,
          disableOnInteraction: false
        }}
        pagination={{ clickable: true }}
        creativeEffect={{
          prev: {
            shadow: true,
            origin: "left center",
            translate: ["-5%", 0, -200],
            rotate: [0, 100, 0]
          },
          next: {
            origin: "right center",
            translate: ["5%", 0, -200],
            rotate: [0, -100, 0]
          }
        }}
      >
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-1.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-2.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-3.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-4.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-5.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-6.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-7.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-8.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-9.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-10.jpg)"
          }}
        ></SwiperSlide>
      </Swiper>
    </div>
  );
};

export default UseAutoplay;
