/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-28 15:02:54
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-28 16:42:13
 * @FilePath: \react-h5\src\components\swiperSlide\effectCoverflow\Index.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */
import React, { useState, useEffect, useRef } from "react";
import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  EffectCoverflow,
  Mousewheel,
  Autoplay
} from "../../../../node_modules/swiper/modules";
import { Grid, Image, Cell } from "react-vant";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

import { handleLoadImage } from "../../../utils/importImg";

const UseAlmostDemo = () => {
  const [swiper, setSwiper] = useState(null);
  const [img, setImg] = useState({});

  useEffect(() => {
    swiper &&
      swiper.on("slideChangeTransitionEnd", function (swiper, event) {
        console.log("slideChangeTransitionEnd", swiper, event);
      });
    swiper &&
      swiper.on("touchEnd", function (swiper, event) {
        console.log("touchEnd", swiper, event);
        if (swiper.autoplay.running) {
          swiper.autoplay.stop();
        } else {
          swiper.autoplay.start();
        }
      });

    return () => {};
  }, [swiper]);

  useEffect(() => {
    setImg(
      handleLoadImage([
        "swiper/nature-1.jpg",
        "swiper/nature-2.jpg",
        "swiper/nature-3.jpg",
        "swiper/nature-4.jpg",
        "swiper/nature-5.jpg",
        "swiper/nature-6.jpg",
        "swiper/nature-7.jpg",
        "swiper/nature-8.jpg",
        "swiper/nature-9.jpg",
        "swiper/nature-0.jpg",
      ])
    );
  }, []);

  return (
    <div className='effect-cover-flow-content'>
      <Swiper
        modules={[Pagination, EffectCoverflow, Navigation, Autoplay]}
        initialSlide={0}
        onSwiper={(swiper) => setSwiper(swiper)}
        effect='coverflow'
        slidesPerView='auto'
        grabCursor={true}
        centeredSlides={true}
        autoplay={{
          delay: 1000,
          pauseOnMouseEnter: true,
          disableOnInteraction: false
        }}
        navigation={true}
        simulateTouch={true}
        coverflowEffect={{
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: true
        }}
        // scrollbar
        // mousewheel={{ forceToAxis: true, sensitivity: 0.1, releaseOnEdges: true }}
        pagination={{ clickable: true, dynamicBullets: true }}
      >
        <SwiperSlide
          style={{
            backgroundImage: `url(${img["swiper/nature-1.jpg"]})`
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: `url(${img["swiper/nature-2.jpg"]})`
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: `url(${img["swiper/nature-3.jpg"]})`
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: `url(${img["swiper/nature-4.jpg"]})`
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: `url(${img["swiper/nature-5.jpg"]})`
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: `url(${img["swiper/nature-6.jpg"]})`
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: `url(${img["swiper/nature-7.jpg"]})`
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: `url(${img["swiper/nature-8.jpg"]})`
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: `url(${img["swiper/nature-9.jpg"]})`
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: `url(${img["swiper/nature-10.jpg"]})`
          }}
        ></SwiperSlide>
      </Swiper>
    </div>
  );
};

export default UseAlmostDemo;
