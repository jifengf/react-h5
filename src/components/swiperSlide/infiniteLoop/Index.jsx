/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-28 09:30:11
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-28 14:30:36
 * @FilePath: \react-h5\src\components\swiperSlide\grid\Index.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */

import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  Mousewheel,
  Grid
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseInfinteLoop = () => {
  return (
    <div className='infinite-loop-content'>
      <Swiper
        modules={[Pagination, Navigation]}
        onSwiper={(swiper) => (window.swiper = swiper)}
        spaceBetween={30} // 间距
        slidesPerView={1} // 展示数量
        pagination={{ clickable: true }}
        loop={true}
        navigation={true}
        autoplay={true}
      >
        <SwiperSlide>Slide 1</SwiperSlide>
        <SwiperSlide>Slide 2</SwiperSlide>
        <SwiperSlide>Slide 3</SwiperSlide>
        <SwiperSlide>Slide 4</SwiperSlide>
        <SwiperSlide>Slide 5</SwiperSlide>
        <SwiperSlide>Slide 6</SwiperSlide>
      </Swiper>
    </div>
  );
};

export default UseInfinteLoop;
