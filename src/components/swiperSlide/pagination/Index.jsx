
import { Swiper, SwiperSlide } from "../../../../node_modules/swiper/swiper-react";
import { Navigation, Pagination, Scrollbar, Mousewheel } from '../../../../node_modules/swiper/modules';

import '../../../../node_modules/swiper/swiper-bundle.css';

import "./style.less";

const UsePagination = () => {

  return (
    <div className='pagination-content'>
    <Swiper
      modules={[ Pagination]}
      onSwiper={(swiper) => (window.swiper = swiper)}
      slidesPerView={1} // 展示数量
      threshold={2} 
      spaceBetween={0} // 间距
      // navigation={true}
      // scrollbar
      // mousewheel={{ forceToAxis: true, sensitivity: 0.1, releaseOnEdges: true }}
      pagination={true}
    >
      <SwiperSlide>Slide 1</SwiperSlide>
      <SwiperSlide>Slide 2</SwiperSlide>
      <SwiperSlide>Slide 3</SwiperSlide>
      <SwiperSlide>Slide 4</SwiperSlide>
      <SwiperSlide>Slide 5</SwiperSlide>
    </Swiper>
    </div>
  );
};

export default UsePagination;
