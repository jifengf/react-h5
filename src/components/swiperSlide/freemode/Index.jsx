/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-28 09:30:11
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-28 13:39:16
 * @FilePath: \react-h5\src\components\swiperSlide\cssMode\Index.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */

import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  Mousewheel
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseFreemode = () => {
  return (
    <div className='freeemode-content'>
    <Swiper
      modules={[Pagination, Mousewheel, Navigation]}
      onSwiper={(swiper) => (window.swiper = swiper)}
      slidesPerView={3} // 展示数量
      freeMode={true}
      // threshold={2}
      spaceBetween={30} // 间距
      // centeredSlides={true}
      navigation={true}
      mousewheel={{ forceToAxis: true, sensitivity: 0.1, releaseOnEdges: true }}     
       keyboard={true}
      pagination={{ clickable: true }}
    >
      <SwiperSlide>Slide 1</SwiperSlide>
      <SwiperSlide>Slide 2</SwiperSlide>
      <SwiperSlide>Slide 3</SwiperSlide>
      <SwiperSlide>Slide 4</SwiperSlide>
      <SwiperSlide>Slide 5</SwiperSlide>
    </Swiper>
    </div>
  );
};

export default UseFreemode;
