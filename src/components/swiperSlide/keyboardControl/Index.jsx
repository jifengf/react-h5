import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  Keyboard,
  Mousewheel
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseKeyboardControl = () => {
  return (
    <div className="keyboard-control-content">
      <Swiper
        modules={[Pagination, Navigation, Keyboard]}
        onSwiper={(swiper) => (window.swiper = swiper)}
        slidesPerView={1} // 展示数量
        spaceBetween={30} // 间距
        navigation={true}
        keyboard={{
          enabled: true
        }}
        pagination={{ clickable: true }}
      >
        <SwiperSlide>Slide 1</SwiperSlide>
        <SwiperSlide>Slide 2</SwiperSlide>
        <SwiperSlide>Slide 3</SwiperSlide>
        <SwiperSlide>Slide 4</SwiperSlide>
        <SwiperSlide>Slide 5</SwiperSlide>
      </Swiper>
    </div>
  );
};

export default UseKeyboardControl;
