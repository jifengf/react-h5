/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-28 15:02:54
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-28 16:29:29
 * @FilePath: \react-h5\src\components\swiperSlide\effectCube\Index.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */
import React from "react";
import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  EffectCube,
  Mousewheel
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseEffectCube = () => {
  return (
    <div className='effect-cube-content'>
      <Swiper
        modules={[Pagination, EffectCube]}
        initialSlide={0}
        // onSwiper={(swiper) => (window.swiper = swiper)}
        effect='cube'
        grabCursor={true}
        loop={true}
        autoplay={{
          delay: 3000
        }}
        observer={true}
        observeParents={true}
        cubeEffect={{
          shadow: true,
          slideShadows: true,
          shadowOffset: 20,
          shadowScale: 0.64
        }}
        // scrollbar
        // mousewheel={{ forceToAxis: true, sensitivity: 0.1, releaseOnEdges: true }}
        pagination={{ clickable: true, dynamicBullets: true }}
      >
        <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-1.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-2.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-3.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-4.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-5.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-6.jpg)"
          }}
        ></SwiperSlide>
      </Swiper>
    </div>
  );
};

export default UseEffectCube;
