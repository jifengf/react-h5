import {
  Swiper,
  SwiperSlide,
  useSwiper
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  Keyboard,
  Mousewheel
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";
import { useState, useEffect } from "react";

const UseChangeDirection = () => {
  const [swiper, setSwiper] = useState(null);
  const getDirection = () => {
    // const windowWidth = window.innerWidth;
    const direction = window.innerWidth <= 760 ? "horizontal" : "vertical";

    return direction;
  };

  useEffect(() => {
    swiper &&
      swiper.on("resize", function () {
        swiper.changeDirection(getDirection());
      });
    return () => {};
  }, [swiper]);

  return (
    <div className="change-direction-content">
      <Swiper
        modules={[Pagination, Navigation, Keyboard]}
        onSwiper={(swiper) => setSwiper(swiper)}
        // onSwiper={(swiper) => setSwiper(window.swiper = swiper)}
        slidesPerView={1} // 展示数量
        spaceBetween={30} // 间距
        navigation={true}
        rewind={true}
        keyboard={{
          enabled: true
        }}
        pagination={{ clickable: true }}
        direction={getDirection()}
      >
        <SwiperSlide>Slide 1</SwiperSlide>
        <SwiperSlide>Slide 2</SwiperSlide>
        <SwiperSlide>Slide 3</SwiperSlide>
        <SwiperSlide>Slide 4</SwiperSlide>
        <SwiperSlide>Slide 5</SwiperSlide>
      </Swiper>
    </div>
  );
};

export default UseChangeDirection;
