/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-28 15:02:54
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-29 11:22:19
 * @FilePath: \react-h5\src\components\swiperSlide\lazyLoadImages\Index.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */
import React from "react";
import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  EffectCoverflow,
  EffectCreative
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseLazyLoadImage = () => {

  return (
    <div className='lazyload-image-content'>
      <Swiper
        modules={[Navigation, Pagination]}
        navigation={true}
        // lazyLoading={true}
        pagination={{ clickable: true }}
      >
        <SwiperSlide>
          <img
            src='https://swiperjs.com/demos/images/nature-1.jpg'
            loading='lazy'
          />
          <div className='swiper-lazy-preloader swiper-lazy-preloader-white'></div>
        </SwiperSlide>
        <SwiperSlide
          loading='lazy'
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-2.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          loading='lazy'
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-3.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          loading='lazy'
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-4.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          loading='lazy'
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-5.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          loading='lazy'
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-6.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          loading='lazy'
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-7.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          loading='lazy'
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-8.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          loading='lazy'
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-9.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          loading='lazy'
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-10.jpg)"
          }}
        ></SwiperSlide>
      </Swiper>
    </div>
  );
};

export default UseLazyLoadImage;
