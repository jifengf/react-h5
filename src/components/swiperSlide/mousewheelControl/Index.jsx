import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  Keyboard,
  Mousewheel
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseMousewheelControl = () => {
  return (
    <div className='mousewheel-control-content'>
      <Swiper
        modules={[Pagination,Mousewheel]}
        onSwiper={(swiper) => (window.swiper = swiper)}
        slidesPerView={1} // 展示数量
        direction={'vertical'} // 
        spaceBetween={30} // 间距
        mousewheel={{ forceToAxis: true, sensitivity: 0.1, releaseOnEdges: true }}
        pagination={{clickable: true}}
      >
        <SwiperSlide>Slide 1</SwiperSlide>
        <SwiperSlide>Slide 2</SwiperSlide>
        <SwiperSlide>Slide 3</SwiperSlide>
        <SwiperSlide>Slide 4</SwiperSlide>
        <SwiperSlide>Slide 5</SwiperSlide>
      </Swiper>
    </div>
  );
};

export default UseMousewheelControl;
