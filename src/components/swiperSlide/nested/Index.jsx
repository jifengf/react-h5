/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-28 09:30:11
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-28 14:35:51
 * @FilePath: \react-h5\src\components\swiperSlide\nested\Index.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */

import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  Mousewheel
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseNested = () => {
  return (
    <div className='nested-content'>
    <Swiper
      modules={[Pagination]}
      onSwiper={(swiper) => (window.swiper = swiper)}
      slidesPerView={1} // 展示数量
      spaceBetween={30} // 间距
      pagination={{ clickable: true }}
    >
      <SwiperSlide>Slide 1</SwiperSlide>
      <SwiperSlide>
        <Swiper
          modules={[Pagination]}
          onSwiper={(swiper) => (window.swiper = swiper)}
          slidesPerView={1} // 展示数量
          direction='vertical'
          spaceBetween={20} // 间距
          pagination={{ clickable: true }}
        >
          <SwiperSlide>Vertical Slide 1</SwiperSlide>
          <SwiperSlide>Vertical Slide 2</SwiperSlide>
          <SwiperSlide>Vertical Slide 3</SwiperSlide>
          <SwiperSlide>Vertical Slide 4</SwiperSlide>
          <SwiperSlide>Vertical Slide 5</SwiperSlide>
        </Swiper>
      </SwiperSlide>
      <SwiperSlide>Slide 3</SwiperSlide>
      <SwiperSlide>Slide 4</SwiperSlide>
      <SwiperSlide>Slide 5</SwiperSlide>
    </Swiper>
    </div>
  );
};

export default UseNested;
