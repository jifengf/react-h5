/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-28 15:02:54
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-28 16:58:58
 * @FilePath: \react-h5\src\components\swiperSlide\effectCreative\Index.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */
import React from "react";
import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  EffectCoverflow,
  EffectCreative
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseEffectCreative = () => {
  return (
    <div className='effect-creative-content'>
      <Swiper
        modules={[EffectCreative]}
        initialSlide={0}
        // onSwiper={(swiper) => (window.swiper = swiper)}
        effect='creative'
        grabCursor={true}
        creativeEffect={{
          prev: {
            shadow: true,
            origin: "left center",
            translate: ["-5%", 0, -200],
            rotate: [0, 100, 0]
          },
          next: {
            origin: "right center",
            translate: ["5%", 0, -200],
            rotate: [0, -100, 0]
          }
        }}
      >
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-1.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-2.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-3.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-4.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-5.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-6.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-7.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-8.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-9.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage:
              "url(https://swiperjs.com/demos/images/nature-10.jpg)"
          }}
        ></SwiperSlide>
      </Swiper>
    </div>
  );
};

export default UseEffectCreative;
