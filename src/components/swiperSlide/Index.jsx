/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-27 16:49:12
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-28 09:21:46
 * @FilePath: \react-h5\src\components\swiperSlide\index.tsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  Mousewheel
} from "../../../node_modules/swiper/modules";

import "../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseSwiperIndex = () => {
  return (
    <div className="swiper-index-content">
      <Swiper
        modules={[Pagination, Mousewheel, Navigation, Scrollbar]}
        onSwiper={(swiper) => (window.swiper = swiper)}
        slidesPerView={1} // 展示数量
        threshold={2}
        spaceBetween={0} // 间距
        navigation={true}
        scrollbar
        mousewheel={{
          forceToAxis: true,
          sensitivity: 0.1,
          releaseOnEdges: true
        }}
        pagination={{ clickable: true, dynamicBullets: true }}
      >
        <SwiperSlide>Slide 1</SwiperSlide>
        <SwiperSlide>Slide 2</SwiperSlide>
        <SwiperSlide>Slide 3</SwiperSlide>
        <SwiperSlide>Slide 4</SwiperSlide>
        <SwiperSlide>Slide 5</SwiperSlide>
      </Swiper>
    </div>
  );
};

export default UseSwiperIndex;
