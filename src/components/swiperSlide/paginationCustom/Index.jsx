/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-28 09:30:11
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-28 09:46:39
 * @FilePath: \react-h5\src\components\swiperSlide\paginationCustom\Index.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */

import { Swiper, SwiperSlide } from "../../../../node_modules/swiper/swiper-react";

import {
  Navigation,
  Pagination,
  Scrollbar,
  Mousewheel
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UsePaginationCustom = () => {
  return (
    <div className="pagination-custom">

    <Swiper
      modules={[Pagination]}
      onSwiper={(swiper) => (window.swiper = swiper)}
      slidesPerView={1} // 展示数量
      threshold={2}
      spaceBetween={0} // 间距
      // navigation={true}
      // scrollbar
      // mousewheel={{ forceToAxis: true, sensitivity: 0.1, releaseOnEdges: true }}
      pagination={{ clickable: true, renderBullet: function (index, className) {
        return '<span class="' + className + '">' + (index + 1) + '</span>';
      },}}
    >
      <SwiperSlide>Slide 1</SwiperSlide>
      <SwiperSlide>Slide 2</SwiperSlide>
      <SwiperSlide>Slide 3</SwiperSlide>
      <SwiperSlide>Slide 4</SwiperSlide>
      <SwiperSlide>Slide 5</SwiperSlide>
    </Swiper>
    </div>
  );
};

export default UsePaginationCustom;
