import { useState } from "react";
import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  Keyboard,
  Mousewheel
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseManipulation = () => {
  let [preppendNum, setPreppendNum] = useState(1);
  let [appendNum, setAppendNum] = useState(6);

  const [list, setList] = useState([
    { key: 1 },
    { key: 2 },
    { key: 3 },
    { key: 4 },
    { key: 5 },
    { key: 6 }
  ]);

  const handleAdd = (type, num) => {
    if (type == "left") {
      let arr = [...list];
      while (num > 0) {
        arr.unshift({ key: --preppendNum });
        setPreppendNum(preppendNum);
        console.log("whilewhilewhile", num, preppendNum);
        num--;
      }
      setList(arr);
    } else if (type == "right") {
      let arr = [...list];
      while (num > 0) {
        arr.push({ key: ++appendNum });
        setAppendNum(appendNum);
        num--;
      }
      setList(arr);
    }
  };

  return (
    <div className='manipulation-content'>
      <Swiper
        modules={[Pagination, Navigation, Keyboard]}
        onSwiper={(swiper) => (window.swiper = swiper)}
        slidesPerView={3} // 展示数量
        spaceBetween={5} // 间距
        centeredSlides={true}
        navigation={true}
        pagination={{ clickable: true }}
      >
        {list.map((item) => {
          return (
            <SwiperSlide key={item.key} virtualIndex={item.key}>{`Slide ${item.key}`}</SwiperSlide>
          );
        })}
      </Swiper>

      <p className='append-buttons'>
        <a
          href='#'
          className='prepend-2-slides'
          onClick={() => handleAdd("left", 2)}
        >
          L + 2
        </a>
        <a
          href='#'
          className='prepend-slide'
          onClick={() => handleAdd("left", 1)}
        >
          L + 1
        </a>
        <a
          href='#'
          className='append-slide'
          onClick={() => handleAdd("right", 1)}
        >
          R + 1
        </a>
        <a
          href='#'
          className='append-2-slides'
          onClick={() => handleAdd("right", 2)}
        >
          R + 2
        </a>
      </p>
    </div>
  );
};

export default UseManipulation;
