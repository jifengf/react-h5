/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-28 15:02:54
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-28 16:42:13
 * @FilePath: \react-h5\src\components\swiperSlide\effectCoverflow\Index.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */
import React from "react";
import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  EffectCoverflow,
  Mousewheel
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseEffectCoverflow = () => {
  return (
    <div className='effect-cover-flow-content'>
      <Swiper
        modules={[Pagination, EffectCoverflow]}
        initialSlide={0}
        // onSwiper={(swiper) => (window.swiper = swiper)}
        effect='coverflow'
        slidesPerView='auto'
        grabCursor={true}
        centeredSlides={true}
        coverflowEffect={{
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: true,
        }}
        // scrollbar
        // mousewheel={{ forceToAxis: true, sensitivity: 0.1, releaseOnEdges: true }}
        pagination={{ clickable: true, dynamicBullets: true }}
      >
        <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-1.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-2.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-3.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-4.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-5.jpg)"
          }}
        ></SwiperSlide>
        <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-6.jpg)"
          }}
        ></SwiperSlide>
         <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-7.jpg)"
          }}
        ></SwiperSlide>
         <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-8.jpg)"
          }}
        ></SwiperSlide>
         <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-9.jpg)"
          }}
        ></SwiperSlide>
         <SwiperSlide
          style={{
            backgroundImage: "url(https://swiperjs.com/demos/images/nature-10.jpg)"
          }}
        ></SwiperSlide>
      </Swiper>
    </div>
  );
};

export default UseEffectCoverflow;
