/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-28 09:30:11
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-28 15:17:42
 * @FilePath: \react-h5\src\components\swiperSlide\slidesPerGroupSkip\Index.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */

import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  Mousewheel,
  Grid
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseSlidesPerGroupSkip = () => {
  return (
    <div className='slidesper-group-skip-content'>
      <Swiper
        modules={[Pagination, Navigation]}
        onSwiper={(swiper) => (window.swiper = swiper)}
        spaceBetween={30} // 间距
        slidesPerGroup={3}
        // loopFillGroupWithBlank={true}
        slidesPerView={1} // 展示数量
        centeredSlides={false}
        pagination={{ clickable: true }}
        loop={true}
        slidesPerGroupSkip={1}
        navigation={true}
        grabCursor={true}
        autoplay={true}
        keyboard={{ enabled: true }}
        breakpoints={{ 769: { slidesPerView: 2, slidesPerGroup: 2 } }}
      >
        <SwiperSlide>Slide 1</SwiperSlide>
        <SwiperSlide>Slide 2</SwiperSlide>
        <SwiperSlide>Slide 3</SwiperSlide>
        <SwiperSlide>Slide 4</SwiperSlide>
        <SwiperSlide>Slide 5</SwiperSlide>
        <SwiperSlide>Slide 6</SwiperSlide>
        <SwiperSlide>Slide 7</SwiperSlide>
        <SwiperSlide>Slide 8</SwiperSlide>
        <SwiperSlide>Slide 9</SwiperSlide>
        <SwiperSlide>Slide 10</SwiperSlide>
        <SwiperSlide>Slide 11</SwiperSlide>
        <SwiperSlide>Slide 12</SwiperSlide>
      </Swiper>
    </div>
  );
};

export default UseSlidesPerGroupSkip;
