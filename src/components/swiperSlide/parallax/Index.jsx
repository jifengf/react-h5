import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  Mousewheel,
  Parallax
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseParallax = () => {
  return (
    <div className='parallax-content'>
      <Swiper
        modules={[Navigation,Parallax]}
        onSwiper={(swiper) => (window.swiper = swiper)}
        slidesPerView={1} // 展示数量
        threshold={2}
        spaceBetween={0} // 间距
        navigation={true}
        parallax={true}
        // scrollbar
        // mousewheel={{ forceToAxis: true, sensitivity: 0.1, releaseOnEdges: true }}
        // pagination={{ clickable: true, dynamicBullets: true }}
      >
        <div
          className='parallax-bg'
          style='background-image:url(https://swiperjs.com/demos/images/nature-1.jpg)'
          data-swiper-parallax='-23%'
        >
          <SwiperSlide>
            {" "}
            <div className='title' data-swiper-parallax='-300'>
              Slide 1
            </div>
            <div className='subtitle' data-swiper-parallax='-200'>
              Subtitle
            </div>
            <div className='text' data-swiper-parallax='-100'>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
                dictum mattis velit, sit amet faucibus felis iaculis nec. Nulla
                laoreet justo vitae porttitor porttitor. Suspendisse in sem
                justo. Integer laoreet magna nec elit suscipit, ac laoreet nibh
                euismod. Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
                ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec,
                tincidunt ut libero. Aenean feugiat non eros quis feugiat.
              </p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            {" "}
            <div
              className='title'
              data-swiper-parallax='-300'
              data-swiper-parallax-opacity='0'
            >
              Slide 2
            </div>
            <div className='subtitle' data-swiper-parallax='-200'>
              Subtitle
            </div>
            <div className='text' data-swiper-parallax='-100'>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
                dictum mattis velit, sit amet faucibus felis iaculis nec. Nulla
                laoreet justo vitae porttitor porttitor. Suspendisse in sem
                justo. Integer laoreet magna nec elit suscipit, ac laoreet nibh
                euismod. Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
                ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec,
                tincidunt ut libero. Aenean feugiat non eros quis feugiat.
              </p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            {" "}
            <div className='title' data-swiper-parallax='-300'>
              Slide 3
            </div>
            <div className='subtitle' data-swiper-parallax='-200'>
              Subtitle
            </div>
            <div className='text' data-swiper-parallax='-100'>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
                dictum mattis velit, sit amet faucibus felis iaculis nec. Nulla
                laoreet justo vitae porttitor porttitor. Suspendisse in sem
                justo. Integer laoreet magna nec elit suscipit, ac laoreet nibh
                euismod. Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
                ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec,
                tincidunt ut libero. Aenean feugiat non eros quis feugiat.
              </p>
            </div>
          </SwiperSlide>
          <SwiperSlide>Slide 4</SwiperSlide>
          <SwiperSlide>Slide 5</SwiperSlide>
        </div>
      </Swiper>
    </div>
  );
};

export default UseParallax;
