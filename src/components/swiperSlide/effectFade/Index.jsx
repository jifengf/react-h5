/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-28 15:02:54
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2023-12-28 15:53:56
 * @FilePath: \react-h5\src\components\swiperSlide\effectFade\Index.jsx
 * @Description:
 *
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved.
 */

import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  EffectFade ,
  Mousewheel
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseEffectFade = () => {
  return (
    <div className='effect-fade-content'>
    <Swiper
      modules={[Navigation, Pagination,EffectFade ]}
      onSwiper={(swiper) => (window.swiper = swiper)}
      spaceBetween={30} // 间距
      navigation={true}
      effect={"fade"}
      // scrollbar
      // mousewheel={{ forceToAxis: true, sensitivity: 0.1, releaseOnEdges: true }}
      pagination={{ clickable: true, dynamicBullets: true }}
    >
      <SwiperSlide
        style={{
          backgroundImage: "url(https://swiperjs.com/demos/images/nature-1.jpg)"
        }}
      ></SwiperSlide>
      <SwiperSlide
        style={{
          backgroundImage: "url(https://swiperjs.com/demos/images/nature-2.jpg)"
        }}
      ></SwiperSlide>
      <SwiperSlide
        style={{
          backgroundImage: "url(https://swiperjs.com/demos/images/nature-3.jpg)"
        }}
      ></SwiperSlide>
      <SwiperSlide
        style={{
          backgroundImage: "url(https://swiperjs.com/demos/images/nature-4.jpg)"
        }}
      ></SwiperSlide>
      <SwiperSlide
        style={{
          backgroundImage: "url(https://swiperjs.com/demos/images/nature-5.jpg)"
        }}
      ></SwiperSlide>
    </Swiper>
    </div>
  );
};

export default UseEffectFade;
