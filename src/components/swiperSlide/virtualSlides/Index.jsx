import { useState, useEffect } from "react";
import {
  Swiper,
  SwiperSlide
} from "../../../../node_modules/swiper/swiper-react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  Keyboard,
  Mousewheel,
  Virtual
} from "../../../../node_modules/swiper/modules";

import "../../../../node_modules/swiper/swiper-bundle.css";

import "./style.less";

const UseVirtualSlides = () => {
  let [preppendNum, setPreppendNum] = useState(1);
  let [appendNum, setAppendNum] = useState(600);
  let [swiper, setSwiper] = useState(null);
  let [type, setType] = useState(null);

  const [list, setList] = useState([]);

  useEffect(() => {
    var slides = [];
    for (var i = 0; i < 600; i += 1) {
      slides.push({ key: i + 1 });
    }

    setList(slides);
  }, []);

  useEffect(() => {
    if (type === "left") {
      swiper.slideTo(0, 0);
    } else if (type === "right") {
      swiper.slideTo(list.length - 1, 0);
    }
  }, [list]);

  const handleAdd = (type, num) => {
    setType(type);
    if (type === "left") {
      let arr = [...list];
      while (num > 0) {
        arr.unshift({ key: --preppendNum });
        setPreppendNum(preppendNum);
        num--;
      }
      setList(arr);
      // swiper.slideTo(0, 0);
    } else if (type === "right") {
      let arr = [...list];
      while (num > 0) {
        arr.push({ key: ++appendNum });
        setAppendNum(appendNum);
        num--;
      }
      setList(arr);
      // swiper.slideTo(arr.length - 1, 0);
    } else if (type === "to") {
      swiper.slideTo(num - 1, 0);
    }
  };

  return (
    <div className="virtual-slides-content">
      <Swiper
        modules={[Pagination, Navigation, Virtual]}
        onSwiper={(swiper) => setSwiper(swiper)}
        slidesPerView={3} // 展示数量
        spaceBetween={5} // 间距
        centeredSlides={true}
        navigation={true}
        pagination={{ clickable: true, type: "fraction" }}
        virtual={true}
      >
        {list.map((slideContent, index) => (
          <SwiperSlide key={slideContent.key} virtualIndex={index}>
            {`Slide ${slideContent.key}`}
          </SwiperSlide>
        ))}
      </Swiper>

      <p className="append-buttons">
        <a
          href="#"
          className="prepend-2-slides"
          onClick={() => handleAdd("left", 2)}
        >
          L + 2
        </a>
        <a
          href="#"
          className="prepend-slide"
          onClick={() => handleAdd("left", 1)}
        >
          L + 1
        </a>
        <a
          href="#"
          className="prepend-slide"
          onClick={() => handleAdd("to", 1)}
        >
          To 1
        </a>
        <a
          href="#"
          className="prepend-slide"
          onClick={() => handleAdd("to", 100)}
        >
          To 100
        </a>
        <a
          href="#"
          className="prepend-slide"
          onClick={() => handleAdd("to", 250)}
        >
          To 250
        </a>
        <a
          href="#"
          className="prepend-slide"
          onClick={() => handleAdd("to", 500)}
        >
          To 500
        </a>
        <a
          href="#"
          className="append-slide"
          onClick={() => handleAdd("right", 1)}
        >
          R + 1
        </a>
        <a
          href="#"
          className="append-2-slides"
          onClick={() => handleAdd("right", 2)}
        >
          R + 2
        </a>
      </p>
    </div>
  );
};

export default UseVirtualSlides;
