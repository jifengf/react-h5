/*
 * @Author:  hpu@foxmail.com
 * @Date: 2023-12-12 15:14:30
 * @LastEditors:  hpu@foxmail.com
 * @LastEditTime: 2024-01-08 14:50:58
 * @FilePath: \react-h5\.eslintrc.cjs
 * @Description: 
 * 
 * Copyright (c) 2023 by hpu@foxmail.com, All Rights Reserved. 
 */
// 修改 .eslintrc.cjs 
module.exports = {
  root: true,
  env: { 
    browser: true,
    commonjs: true,
    es6: true,
    node: true,
    es2020: true 
  },
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:react-hooks/recommended"
  ],
  ignorePatterns: ["dist", ".eslintrc.cjs"],
  parser: "@typescript-eslint/parser",
  plugins: ["react-refresh"],
  rules: {
    "react-refresh/only-export-components": [
      "warn",
      { allowConstantExport: true }
    ],
    // **** 新增 ****
    quotes: [2, "double"], // 必须使用双引号
    semi: [2, "always"], // 语句强制分号结尾
    "comma-dangle": [2, "never"], // 数组和对象键值对最后一个逗号， never参数：不能带末尾的逗号, always参数：必须带末尾的逗号
    eqeqeq: [2, "allow-null"] // 必须使用 ===
  }
};
