# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

# react-h5

## 🌈 简介

react-h5 是一个前端免费开源的移动端（H5）项目，使用 `react` `mobx` `react-router` `react-vant` `vite` 等前端前沿技术开发，也可用于学习参考。

## ✨ 特性

- 💪 使用前端主流技术栈搭建


安装依赖

```bash
# 通过 npm 安装
npm install
```

本地运行

```bash
npm run dev
```

打包发布

```bash
npm run build
```

